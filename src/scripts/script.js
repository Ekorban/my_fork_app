const burgerBtn = document.querySelector('.page-header__menu-burger');
const menuBtn = document.querySelector('.page-header__menu');
const exitBtn = document.querySelector('.page-header__exit-button')

burgerBtn.onclick = function () {
    burgerBtn.classList.add('burger-hide')
    exitBtn.classList.add('active-button')
    menuBtn.classList.add('active-menu')
}

exitBtn.onclick = function () {
    burgerBtn.classList.remove('burger-hide')
    exitBtn.classList.remove('active-button')
    menuBtn.classList.remove('active-menu')
}
/*************************************************/

const tabNav = document.querySelectorAll('.page-header__menu-link');


tabNav.forEach(function (item){
    item.addEventListener("click", function () {
        let currentBtn = item;
        if (! currentBtn.classList.contains('page-header__menu-link--active')) {
            tabNav.forEach(function (item) {
                item.classList.remove('page-header__menu-link--active');
            });
            currentBtn.classList.add('page-header__menu-link--active');
        }
    });
});