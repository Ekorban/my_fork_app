#Fork App file manager
* Fork App is a morden open-sourse editor.


## Current project team members:
Olena Korban (ekorban1982@gmail.com)

## Work done by Elena Korban:
* Develop the structure of index.html
* Develop the site header with the top menu (including the drop-down menu at low screen resolution.
* Create a People Are Talking About Fork section.


## In Fork App were used next toolkits: 
* gulp
* gulp-sass
* browser-sync
* gulp-js-minify
* gulp-clean-css
* gulp-clean
* gulp-concat
* gulp-imagemin
* gulp-autoprefixer
* gulp-htmlmin


